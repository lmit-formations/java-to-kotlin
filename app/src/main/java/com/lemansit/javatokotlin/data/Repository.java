package com.lemansit.javatokotlin.data;

import com.google.gson.Gson;

import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {

    private ApiServices api;

    public ApiServices getApi() {
        return api;
    }

    public Repository() {

//        OkHttpClient clientOkHttp = new OkHttpClient.Builder().build();

        api = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
                .create(ApiServices.class);

    }
}
