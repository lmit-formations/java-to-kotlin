package com.lemansit.javatokotlin.presentation.main;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lemansit.javatokotlin.R;
import com.lemansit.javatokotlin.presentation.posts.PostsActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.btnPosts).setOnClickListener( v -> {
            Intent intent = new Intent(this, PostsActivity.class);
            startActivity(intent);
        });
    }


}
