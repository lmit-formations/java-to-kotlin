package com.lemansit.javatokotlin.presentation.posts;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.lemansit.javatokotlin.R;
import com.lemansit.javatokotlin.data.model.Post;

import java.util.List;

public class PostsActivity extends AppCompatActivity implements PostsPresenter.View {

    private PostsPresenter presenter;

    private PostsAdapter rvAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);
        presenter = new PostsPresenter();
        presenter.attachView(this);

        initRecyclerView();
    }

    private void initRecyclerView() {
        RecyclerView rv = findViewById(R.id.rvPosts);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rvAdapter = new PostsAdapter();
        rv.setAdapter(rvAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        presenter.update();
    }

    @Override
    public void displayPosts(List<Post> posts) {
        rvAdapter.setData(posts);
    }

    @Override
    public void setLoading(boolean state) {
        if (state) {
            findViewById(R.id.progress).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.progress).setVisibility(View.GONE);
        }

    }

    @Override
    public void displayError(String message) {
        new AlertDialog.Builder(this)
                .setTitle("Erreur")
                .setMessage(message)
                .create()
                .show();
    }
}
