package com.lemansit.javatokotlin.presentation.posts;

import com.lemansit.javatokotlin.data.Repository;
import com.lemansit.javatokotlin.data.model.Post;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class PostsPresenter {

    public interface View {
        void displayPosts(List<Post> posts);

        void setLoading(boolean state);

        void displayError(String message);
    }

    private View view;

    public void attachView(View theView) {
        view = theView;
    }


    public void update() {

        Repository repository = new Repository();


        repository.getApi().fetchPosts()
                .observeOn(AndroidSchedulers.mainThread())
                .delay(10, TimeUnit.SECONDS)
                .doOnSubscribe(d -> {
                    view.setLoading(true);
                })
                .doAfterTerminate(() -> view.setLoading(false))
                .subscribe(posts -> {
                    view.displayPosts(posts);
                }, t -> view.displayError(t.getMessage()));
    }


}
