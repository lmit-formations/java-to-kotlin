package com.lemansit.javatokotlin.presentation.posts;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.lemansit.javatokotlin.R;
import com.lemansit.javatokotlin.data.model.Post;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {

    private List<Post> data;

    public void setData(List<Post> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PostViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PostViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull PostViewHolder holder, int position) {
        holder.bindTo(this.data.get(position));
    }

    @Override
    public int getItemCount() {
        return this.data.size();
    }

    static class PostViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private TextView tvUserId;

        public PostViewHolder(ViewGroup parent) {
            super(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false));
            tvTitle = this.itemView.findViewById(R.id.tvTitle);
            tvUserId = this.itemView.findViewById(R.id.tvUserId);
        }

        private void bindTo(Post aPost) {
            tvTitle.setText(aPost.getTitle());
            tvUserId.setText(aPost.getUserId());
        }
    }


}
