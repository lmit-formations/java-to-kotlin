package com.lemansit.javatokotlin.data;

import com.lemansit.javatokotlin.data.model.Post;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;

public interface ApiServices {

    @GET("/posts")
    public Single<List<Post>> fetchPosts();

}
